const utils = {
  Halolemonde() {
    // eslint-disable-next-line no-console
    console.log('salut la terre')
  },
  Scrollanimationstart() {
    // constS
    // Stockage des noms de sélecteurs
    const $ = {
      module: 'data-js-animate'
    }
    /**
     * @description "Regarde" la présence des modules devant être animés dans le viewport afin d'activer une classe sur l'item correspondant dans le menu
     * @see https://alligator.io/js/intersection-observer
     */

    const watchModules = function watchModules() {
      // Pour chaque entrées, si elle est présente dans le viewport, ajouter la classe active sur l'item correspondant dans le menu
      const observer = new IntersectionObserver(function(entries) {
        entries.forEach(function(_ref) {
          const target = _ref.target
          const isIntersecting = _ref.isIntersecting

          if (isIntersecting) {
            target.classList.add('animate')
            observer.unobserve(target)
          }
        })
      })

      const watch = function watch() {
        document
          .querySelectorAll('['.concat($.module, ']'))
          .forEach(function(element) {
            observer.observe(element)
          })
      }

      watch()
    }

    // GO 🚀
    watchModules() // constS
  },
  Scrollanimation() {
    const myViewport = {
      top: innerHeight * 0.4,
      bottom: innerHeight * 0.6
    }
    const animateItem = document.querySelectorAll('[data-js-scroll]')
    const isIE11 = !!window.MSInputMethodContext && !!document.documentMode // true on IE11
    // false on Edge and other IEs/browsers.

    function inViewport(element) {
      const viewport =
        arguments.length > 1 && arguments[1] !== undefined
          ? arguments[1]
          : {
              top: 0,
              bottom: innerHeight
            }
      // Get the elements position relative to the viewport
      const bb = element.getBoundingClientRect() // Check if the element is outside the viewport
      // Then invert the returned value because you want to know the opposite

      return !(bb.top > viewport.bottom || bb.bottom < viewport.top)
    }

    window.addEventListener('resize', function(event) {
      // Update your viewport values
      myViewport.top = innerHeight * 0.4
      myViewport.bottom = innerHeight * 0.6
    }) // Get all items with data-attribute "data-js-scroll"

    const addScroll = function addScroll() {
      animateItem.forEach(function(el) {
        // Listen for the scroll event
        document.addEventListener('scroll', function(event) {
          // Check the viewport status
          if (inViewport(el, myViewport)) el.classList.add('scrolled') // else el.classList.remove('scrolled');
        })
      })
    }

    // GO
    if (!isIE11) addScroll()
  }
}

export default utils
