export default {
  mode: 'universal',
  /*
   ** Headers of the page
   */
  head: {
    title: 'Lenny Mans : Développeur Front-End - Portfolio',
    htmlAttrs: {
      lang: 'fr'
    },
    meta: [
      {
        charset: 'utf-8'
      },
      {
        name: 'viewport',
        content: 'width=device-width, initial-scale=1'
      },
      {
        hid: 'description',
        name: 'description',
        content: process.env.npm_package_description || ''
      }
    ],
    link: [
      {
        rel: 'icon',
        type: 'image/x-icon',
        href: '/mountain.ico'
      }
    ]
  },
  /*
   ** Customize the progress-bar color
   */
  loading: {
    color: '#fff'
  },
  /*
   ** Global CSS
   */
  css: ['@/assets/scss/theme.scss'],
  /*
   ** Plugins to load before mounting the App
   */
  plugins: ['~/plugins/main.js'],
  /*
   ** Nuxt.js dev-modules
   */
  buildModules: [
    // Doc: https://github.com/nuxt-community/eslint-module
    '@nuxtjs/eslint-module'
  ],
  /*
   ** Nuxt.js modules
   */
  modules: [
    // Doc: https://bootstrap-vue.js.org
    'bootstrap-vue/nuxt',
    '@nuxtjs/robots',
    'vue-scrollto/nuxt'
  ],
  /*
   ** Robots options
   */
  robots: {
    UserAgent: '*',
    Disallow: '/user'
  },
  /*
   ** Build configuration
   */
  build: {
    /*
     ** You can extend webpack config here
     */
    extend(config, ctx) {}
  }
}
